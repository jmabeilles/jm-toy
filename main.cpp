#include <iostream>
#include <string>


using namespace std;

void printit(const std::string& s){
    cout << "hello: " << s << std::endl;
    return;
}

int main(){
    std::cout << "hello, world\n";

    string s;
    cout << "what's your name? ";
    cin >> s;
    printit(s);
    return 0;
}